#!/bin/sh

echo "Bitcoin-RT compile windows script by DanielMazurFL4RE - Ubuntu 20.04 required"
echo "Set POSIX mingw"
sudo update-alternatives --config i686-w64-mingw32-g++ 
cd depends
make HOST=i686-w64-mingw32 -j4
cd ..
make clean
./autogen.sh
CONFIG_SITE=$PWD/depends/i686-w64-mingw32/share/config.site ./configure --prefix=/
make -j4

